package org.icec.web.ireport.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
@RequestMapping("ireport")
public class IreportController {
	/**
	 * 浏览器直接显示演示
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("showpdf")
	public void showpdf(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//parameters设置
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("a", "导出pdf测试");

		// Field变量
		List<String> list = new ArrayList<String>();
		list.add("ddd");
		list.add("非常好");

		String jrxml = "/test.jrxml";

		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		response.setContentType("application/pdf");
		InputStream input = IreportController.class.getResourceAsStream("/jasperreports" + jrxml);
		JasperReport report = JasperCompileManager.compileReport(input);
		JRDataSource ds = new JREmptyDataSource();
		if (!CollectionUtils.isEmpty(list)) {
			ds = new JRBeanCollectionDataSource(list);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, ds);
		ServletOutputStream ouputStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, ouputStream);
		ouputStream.flush();
		ouputStream.close();
	}
	/**
	 * 下载pdf演示
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("exportpdf")
	public void exportpdf(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//parameters设置
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("a", "导出pdf测试");

		// Field变量
		List<String> list = new ArrayList<String>();
		list.add("ddd");
		list.add("非常好");

		String jrxml = "/test.jrxml";

		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		response.setContentType("application/pdf");
		String defaultname = "pdf测试.pdf";
		String fileName = new String(defaultname.getBytes("GBK"), "ISO8859_1");
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);
		InputStream input = IreportController.class.getResourceAsStream("/jasperreports" + jrxml);
		JasperReport report = JasperCompileManager.compileReport(input);
		JRDataSource ds = new JREmptyDataSource();
		if (!CollectionUtils.isEmpty(list)) {
			ds = new JRBeanCollectionDataSource(list);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, ds);
		ServletOutputStream ouputStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, ouputStream);
		ouputStream.flush();
		ouputStream.close();
	}
}
