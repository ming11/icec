getRecordByState
===

	select id,template_name,phone,( select content from sms_template where code = template_code and del_flag=0 )as template_code,param from sms_record where del_flag=0 and state=#state#

pageQuery
===
* 分页查询
 
	select
	@pageTag(){
	#use("cols")#  
	@} 
	from  sms_record
	where #use("condition")#

sample
===
* 注释

	select #use("cols")# from sms_record where #use("condition")#

cols
===

	id,template_name,phone,template_code,param,state,sendby,createtime,sendtime,note,del_flag

updateSample
===

	id=#id#,template_name=#templateName#,phone=#phone#,template_code=#templateCode#,param=#param#,state=#state#,sendby=#sendby#,createtime=#createtime#,sendtime=#sendtime#,note=#note#,del_flag=#delFlag#

condition
===

	 1 = 1  
@if(!isEmpty(templateName)){
 and template_name=#templateName#
@}
@if(!isEmpty(phone)){
 and phone=#phone#
@}
@if(!isEmpty(templateCode)){
 and template_code=#templateCode#
@}
@if(!isEmpty(param)){
 and param=#param#
@}
@if(!isEmpty(state)){
 and state=#state#
@}
@if(!isEmpty(sendby)){
 and sendby=#sendby#
@}
@if(!isEmpty(createtime)){
 and createtime=#createtime#
@}
@if(!isEmpty(sendtime)){
 and sendtime=#sendtime#
@}
@if(!isEmpty(note)){
 and note=#note#
@}
@if(!isEmpty(delFlag)){
 and del_flag=#delFlag#
@}
