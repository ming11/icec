pageQuery
===
* 分页查询
 
	select
	@pageTag(){
	#use("cols")#  
	@} 
	from  sms_template
	where #use("condition")#

sample
===
* 注释

	select #use("cols")# from sms_template where #use("condition")#

cols
===

	id,name,code,content,note,create_by,create_time,update_by,update_time,del_flag

updateSample
===

	id=#id#,name=#name#,code=#code#,content=#content#,note=#note#,create_by=#createBy#,create_time=#createTime#,update_by=#updateBy#,update_time=#updateTime#,del_flag=#delFlag#

condition
===

	 1 = 1  
@if(!isEmpty(name)){
 and name=#name#
@}
@if(!isEmpty(code)){
 and code=#code#
@}
@if(!isEmpty(content)){
 and content=#content#
@}
@if(!isEmpty(note)){
 and note=#note#
@}
@if(!isEmpty(createBy)){
 and create_by=#createBy#
@}
@if(!isEmpty(createTime)){
 and create_time=#createTime#
@}
@if(!isEmpty(updateBy)){
 and update_by=#updateBy#
@}
@if(!isEmpty(updateTime)){
 and update_time=#updateTime#
@}
@if(!isEmpty(delFlag)){
 and del_flag=#delFlag#
@}
