package org.icec.sms.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.icec.common.model.BaseModel;
import org.icec.sms.model.SmsTemplate;
import org.icec.web.core.sys.model.SysUser;
import org.icec.sms.dao.SmsTemplateDao;

/*
* 
* gen by jinxx  2018-12-12
*/
@Service
public class SmsTemplateService {
	@Autowired
	private SmsTemplateDao smsTemplateDao;

	/**
	 *
	 * 保存
	 */
	@Transactional
	public void save(SmsTemplate smsTemplate, SysUser optuser) {
		smsTemplate.setDelFlag(SmsTemplate.DEL_FLAG_NORMAL);
		smsTemplate.setCreateTime(new Date());
		smsTemplate.setCreateBy(optuser.getId());
		smsTemplateDao.insert(smsTemplate);
	}

	/**
	 *
	 * 更新
	 */
	@Transactional
	public void update(SmsTemplate smsTemplate, SysUser optuser) {
		smsTemplate.setUpdateBy(optuser.getId());
		smsTemplate.setUpdateTime(new Date());
		smsTemplateDao.updateTemplateById(smsTemplate);
	}

	/**
	 * 删除操作，更新del_flag字段
	 * 
	 * @param ids
	 * @param optuser
	 */
	@Transactional
	public void deleteAll(String ids, SysUser optuser) {
		String[] idarr = ids.split(",");
		for (String id : idarr) {
			SmsTemplate smsTemplate = new SmsTemplate();
			smsTemplate.setId(Integer.parseInt(id));
			smsTemplate.setDelFlag(BaseModel.DEL_FLAG_DELETE);
			smsTemplate.setUpdateBy(optuser.getId());
			smsTemplate.setUpdateTime(new Date());
			smsTemplateDao.updateTemplateById(smsTemplate);
		}

	}

	/**
	 *
	 * 按主键查询
	 *
	 */
	public SmsTemplate get(Integer id) {
		return smsTemplateDao.single(id);
	}

	/**
	 * 查詢全部记录
	 * @return
	 */
	public List<SmsTemplate> getAll() {
		return smsTemplateDao.createLambdaQuery()
				.andEq(SmsTemplate::getDelFlag, SmsTemplate.DEL_FLAG_NORMAL)
				.select();
	}

	/**
	 *
	 * 分页查询
	 *
	 */
	public PageQuery<SmsTemplate> pageQuery(PageQuery<SmsTemplate> query) {
		return smsTemplateDao.pageQuery(query);
	}
}
