package org.icec.sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
@Component
@PropertySource(value = { "classpath:alisms.properties" })
@ConfigurationProperties(prefix = "icec.alisms")
@Getter @Setter
public class SmsProperties {
	private String accessKeyId;
	private String accessKeySecret;
	private String connectTimeout;
	private String readTimeout;
	private String signName;
}
