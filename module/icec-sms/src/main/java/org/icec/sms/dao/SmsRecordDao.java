package org.icec.sms.dao;
import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.icec.sms.model.SmsRecord;


/* 
* 
* gen by icec mapper 2018-10-29
*/
public interface SmsRecordDao extends BaseMapper<SmsRecord> {
	/*
	*
	*分页查询
	*
	*/
	public PageQuery<SmsRecord> pageQuery(PageQuery<SmsRecord> query);
	/**
	 * 根据发送状态查询
	 * @param state
	 * @return
	 */
	public List<SmsRecord> getRecordByState(@Param("state") String state);
}
