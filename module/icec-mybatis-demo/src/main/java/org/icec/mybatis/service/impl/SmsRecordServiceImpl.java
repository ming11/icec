package org.icec.mybatis.service.impl;

import java.util.List;

import org.icec.mybatis.domain.SmsRecord;
import org.icec.mybatis.mapper.SmsRecordMapper;
import org.icec.mybatis.service.ISmsRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 短信记录Service业务层处理
 * 
 * @author jinxx
 * @date 2020-09-14
 */
@Service
public class SmsRecordServiceImpl implements ISmsRecordService 
{
    @Autowired
    private SmsRecordMapper smsRecordMapper;

    /**
     * 查询短信记录
     * 
     * @param id 短信记录ID
     * @return 短信记录
     */
    @Override
    public SmsRecord selectSmsRecordById(Long id)
    {
        return smsRecordMapper.selectSmsRecordById(id);
    }

    /**
     * 查询短信记录列表
     * 
     * @param smsRecord 短信记录
     * @return 短信记录
     */
    @Override
    public List<SmsRecord> selectSmsRecordList(SmsRecord smsRecord)
    {
        return smsRecordMapper.selectSmsRecordList(smsRecord);
    }

    /**
     * 新增短信记录
     * 
     * @param smsRecord 短信记录
     * @return 结果
     */
    @Override
    public int insertSmsRecord(SmsRecord smsRecord)
    {
        return smsRecordMapper.insertSmsRecord(smsRecord);
    }

    /**
     * 修改短信记录
     * 
     * @param smsRecord 短信记录
     * @return 结果
     */
    @Override
    public int updateSmsRecord(SmsRecord smsRecord)
    {
        return smsRecordMapper.updateSmsRecord(smsRecord);
    }

    /**
     * 删除短信记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSmsRecordByIds(String ids)
    {
        return 0;
    }

    /**
     * 删除短信记录信息
     * 
     * @param id 短信记录ID
     * @return 结果
     */
    @Override
    public int deleteSmsRecordById(Long id)
    {
        return smsRecordMapper.deleteSmsRecordById(id);
    }
}
