package org.icec.mybatis.controller;

import java.util.List;

import org.icec.common.base.tips.Tip;
import org.icec.common.web.BaseController;
import org.icec.mybatis.domain.SmsRecord;
import org.icec.mybatis.service.ISmsRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;


/**
 * 短信记录Controller
 * 
 * @author jinxx
 * @date 2020-09-14
 */
@Controller
@RequestMapping("/sms/smsRecord")
@Slf4j
public class SmsRecordController extends BaseController
{
    private String prefix = "sms/smsRecord";

    @Autowired
    private ISmsRecordService smsRecordService;

    @GetMapping()
    public String smsRecord()
    {
        return prefix + "/smsRecord";
    }

    /**
     * 查询短信记录列表
     */
    @GetMapping("/list")
    @ResponseBody
    public Tip list(SmsRecord smsRecord)
    {
        List<SmsRecord> list = smsRecordService.selectSmsRecordList(smsRecord);
        log.info(list.size()+"");
        return  null;
    }

   
 
   

   
}
