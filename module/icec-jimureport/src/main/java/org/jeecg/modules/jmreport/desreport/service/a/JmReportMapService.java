package org.jeecg.modules.jmreport.desreport.service.a;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.jmreport.desreport.entity.JmReportMap;
import org.jeecg.modules.jmreport.desreport.mapper.JmReportMapMapper;
import org.jeecg.modules.jmreport.desreport.service.IJmReportMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service("jmReportMapServiceImpl")
@Primary 
public class JmReportMapService extends ServiceImpl<JmReportMapMapper, JmReportMap> implements IJmReportMapService {
  
  
  
  public void saveMapSource(JmReportMap map) {
     saveOrUpdate(map);
  }
}
