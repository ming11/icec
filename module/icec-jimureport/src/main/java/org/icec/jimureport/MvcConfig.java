package org.icec.jimureport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

@Configuration
@EnableAutoConfiguration
public class MvcConfig implements WebMvcConfigurer {
	@Autowired
	ThymeleafViewResolver myThymeleafViewResolver;
	//@Bean
	public FreeMarkerViewResolver freeMarkerViewResolver() {
		MyFreeMarkerViewResolver resolver = new MyFreeMarkerViewResolver();

		resolver.setPrefix(null);

		resolver.setSuffix(".ftl");

		resolver.setContentType("text/html; charset=UTF-8");

		resolver.setRequestContextAttribute("request");

		resolver.setExposeContextBeansAsAttributes(true);

		resolver.setExposeRequestAttributes(true);

		resolver.setExposeSessionAttributes(true);

		resolver.setOrder(1);

		return resolver;

	}
	//@Bean
	public MyThymeleafViewResolver myThymeleafViewResolver() {
		MyThymeleafViewResolver resolver2 = new MyThymeleafViewResolver();
		BeanUtils.copyProperties(myThymeleafViewResolver, resolver2);
		resolver2.setOrder(1);
		return resolver2;

	}
}