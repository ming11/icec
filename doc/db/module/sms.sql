drop table if exists sms_record;

/*==============================================================*/
/* Table: sms_record                                            */
/*==============================================================*/
create table sms_record
(
   id                   int not null auto_increment,
   template_name        varchar(255) comment '如登录，催款，提醒',
   phone                varchar(255) comment '手机号码',
   template_code        varchar(255) comment '短信模板编码',
   param                varchar(255) comment '短信模板参数json格式',
   state                varchar(1) comment '0未发送，1已发送',
   sendby               int,
   createtime           datetime,
   sendtime             datetime,
   note                 varchar(1),
   del_flag             varchar(1),
   code                 varchar(255),
   primary key (id)
);


drop table if exists sms_template;

/*==============================================================*/
/* Table: sms_template  短信模板表                                        */
/*==============================================================*/
create table sms_template
(
   id                   int not null auto_increment,
   name                 varchar(255) comment '名称',
   code                 varchar(255) comment '内部编码',
   content              varchar(255) comment '对应阿里云模板编码',
   note                 varchar(255) comment '备注',
   create_by            int comment '创建人',
   create_time          datetime,
   update_by            int,
   update_time          datetime,
   del_flag             varchar(1) comment '删除标识',
   primary key (id)
);
