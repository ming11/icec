package org.icec.common.constants;

public class CacheConstants {
	/**
	 * 字典缓存
	 */
	public static final String dictionary="dictionary";
	/**
	 * 常用系统对象缓存
	 */
	public static final String systemCache="systemCache";
}
