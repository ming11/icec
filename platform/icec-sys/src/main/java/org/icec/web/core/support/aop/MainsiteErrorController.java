package org.icec.web.core.support.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.icec.common.base.tips.ErrorTip;
import org.icec.common.utils.AjaxUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
@Controller
public class MainsiteErrorController extends AbstractErrorController {
	private static final String ERROR_PATH = "/error";
	@Autowired
	ObjectMapper objectMapper;
	
	public MainsiteErrorController() {
		super(new DefaultErrorAttributes());
	}
	@RequestMapping(value = ERROR_PATH)
	public String handleError(HttpServletRequest request, HttpServletResponse response) {
		if(AjaxUtils.isAjaxRequest(request)){//ajax 则返回json
	   		 AjaxUtils.writeJson(new ErrorTip(404,"地址不存在"),response);
	   		 return null;
	   	}
		return "error/404";
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

}
