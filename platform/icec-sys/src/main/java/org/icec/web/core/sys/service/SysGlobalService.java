package org.icec.web.core.sys.service;
import org.springframework.stereotype.Service;
import org.icec.common.constants.CacheConstants;
import org.icec.web.core.sys.dao.SysGlobalDao;
import org.icec.web.core.sys.model.SysGlobal;
import org.icec.web.core.sys.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/*
* 
* gen by icec  2018-01-16
*/
@Service
public class SysGlobalService   {
	@Autowired
	private SysGlobalDao  sysGlobalDao ;
	@Autowired
	private SysFileService sysFileService;
	/**
	*
	*更新全局
	*/
	@CacheEvict(value=CacheConstants.systemCache,key="'sysGlobal'")
	@Transactional
	public void update(SysGlobal sysGlobal,SysUser optuser, MultipartFile multiFile){
		sysGlobal.setId(1);
		sysGlobal.setCreateTime(new Date());
		Long result = sysFileService.saveUploadFile("logo", multiFile, optuser);
		sysGlobal.setLogo(result);
		sysGlobalDao.updateById(sysGlobal);
	}
	
	/**
	*
	*按主键查询
	*
	*/
	@Cacheable(value=CacheConstants.systemCache,key="'sysGlobal'")
	public SysGlobal getGlobal(){
		SysGlobal global= sysGlobalDao.single(1);
		if (global == null) {
			throw new IllegalStateException("Global not exist!");
		}
		return global;
	}
}
