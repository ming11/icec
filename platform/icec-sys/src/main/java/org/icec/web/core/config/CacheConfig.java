package org.icec.web.core.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
/**
 * 开启缓存
 * @author xxjin
 *
 */
@Configuration
@EnableCaching
public class CacheConfig {
	
}
