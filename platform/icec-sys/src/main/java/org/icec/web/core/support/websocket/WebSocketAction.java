package org.icec.web.core.support.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebSocketAction {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @MessageExceptionHandler  
    public void handleExceptions(Throwable t) {  
    	logger.error("Error handling message: " + t.getMessage());  
    }
    @GetMapping("websocket/test")
    public String test() {
    	return "websocket/test";
    }
}
