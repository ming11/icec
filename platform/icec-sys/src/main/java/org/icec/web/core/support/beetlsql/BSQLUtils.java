package org.icec.web.core.support.beetlsql;

import java.sql.SQLException;

import org.beetl.sql.core.db.DB2SqlStyle;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.H2Style;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.core.db.OracleStyle;
import org.beetl.sql.core.db.PostgresStyle;
import org.beetl.sql.core.db.SQLiteStyle;
import org.beetl.sql.core.db.SqlServerStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.util.JdbcConstants;

/**
 * beetlsql工具类
 * 
 * @author xxjin
 *
 */
public class BSQLUtils {
	private static Logger logger = LoggerFactory.getLogger(BSQLUtils.class);

	public static DBStyle getDBStyleFromDriveName(String dbType) throws SQLException {
		switch (dbType) {
		case JdbcConstants.MYSQL:
			return new MySqlStyle();
		case JdbcConstants.POSTGRESQL:
			return new PostgresStyle();
		case JdbcConstants.ORACLE:
			return new OracleStyle();
		case JdbcConstants.ALI_ORACLE:
			return new OracleStyle();
		case JdbcConstants.DB2:
			return new DB2SqlStyle();
		case JdbcConstants.SQL_SERVER:
			return new SqlServerStyle();
		case JdbcConstants.SQLITE:
			return new SQLiteStyle();
		case JdbcConstants.H2:
			return new H2Style();

		default:
			logger.warn("db type  is not support! the name is {}", dbType);
			throw new SQLException("dbType  is not support! : " + dbType);
		}
	}
}
