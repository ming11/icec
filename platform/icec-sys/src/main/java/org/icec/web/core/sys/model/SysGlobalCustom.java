package org.icec.web.core.sys.model;
import org.icec.common.model.BaseModel;

/*
* 
* gen by icec 2018-12-25
*/
public class SysGlobalCustom   implements BaseModel{
	//id
	private Integer id ;
	//键
	private String fKey ;
	//说明
	private String fNote ;
	//前缀，名称
	private String fPrefix ;
	//值
	private String fValue ;
	
	public SysGlobalCustom() {
	}
	
	public Integer getId(){
		return  id;
	}
	public void setId(Integer id ){
		this.id = id;
	}
	
	public String getfKey(){
		return  fKey;
	}
	public void setfKey(String fKey ){
		this.fKey = fKey;
	}
	
	public String getfNote(){
		return  fNote;
	}
	public void setfNote(String fNote ){
		this.fNote = fNote;
	}
	
	public String getfPrefix(){
		return  fPrefix;
	}
	public void setfPrefix(String fPrefix ){
		this.fPrefix = fPrefix;
	}
	
	public String getfValue(){
		return  fValue;
	}
	public void setfValue(String fValue ){
		this.fValue = fValue;
	}
	
	
	

}
