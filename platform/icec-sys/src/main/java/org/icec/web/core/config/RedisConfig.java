package org.icec.web.core.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonAutoDetect;


@Configuration
@ConditionalOnClass(value=RedisTemplate.class)
public class RedisConfig {
	@Bean  
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {  
	    RedisTemplate<String, Object> template = new RedisTemplate<>();  
	    template.setConnectionFactory(factory);  
	    Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);  
	    RedisSerializer<String> stringSerializer = new StringRedisSerializer();  
	    ObjectMapper om = new ObjectMapper();  
	    om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);  
	    om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);  
	    jackson2JsonRedisSerializer.setObjectMapper(om);  
	    template.setValueSerializer(jackson2JsonRedisSerializer);  
	    template.setKeySerializer(stringSerializer);  
	    template.afterPropertiesSet();  
	    return template;  
	}  
	  
	@Bean  
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory factory) {  
	    StringRedisTemplate template = new StringRedisTemplate(factory);  
	    RedisSerializer<String> stringSerializer = new StringRedisSerializer();  
	    template.setKeySerializer(stringSerializer);  
	    template.setValueSerializer(stringSerializer);  
	    template.setHashKeySerializer(stringSerializer);  
	    template.setKeySerializer(stringSerializer);  
	    template.afterPropertiesSet();  
	    return template;  
	} 
}
